<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Indicator;




class IndicatorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Indicator::factory()->count(5)->create();
    }
}
